{ The GraphicsMagic -- an image manipulation program
  CopyRight(C) 2001-2010, Ma Xiaoguang & Ma Xiaoming < gmbros@hotmail.com >.
  All rights reserved. }

program GraphicsMagic;

(* ***** BEGIN LICENSE BLOCK *****
 * Version: MPL 1.1/LGPL 2.1/GPL 2.0
 *
 * The contents of this file are subject to the Mozilla Public License Version
 * 1.1 (the "License"); you may not use this file except in compliance with
 * the License. You may obtain a copy of the License at
 * http://www.mozilla.org/MPL/
 *
 * Software distributed under the License is distributed on an "AS IS" basis,
 * WITHOUT WARRANTY OF ANY KIND, either express or implied. See the License
 * for the specific language governing rights and limitations under the
 * License.
 *
 * The Initial Developer of this unit are
 *
 * Ma Xiaoguang and Ma Xiaoming < gmbros@hotmail.com >
 *
 * Contributor(s):
 * Andre Felix Miertschink
 *
 * Alternatively, the contents of this file may be used under the terms of
 * either the GNU General Public License Version 2 or later (the "GPL"), or
 * the GNU Lesser General Public License Version 2.1 or later (the "LGPL"),
 * in which case the provisions of the GPL or the LGPL are applicable instead
 * of those above. If you wish to allow use of your version of this file only
 * under the terms of either the GPL or the LGPL, and not to allow others to
 * use your version of this file under the terms of the MPL, indicate your
 * decision by deleting the provisions above and replace them with the notice
 * and other provisions required by the GPL or the LGPL. If you do not delete
 * the provisions above, a recipient may use your version of this file under
 * the terms of any one of the MPL, the GPL or the LGPL.
 * ***** END LICENSE BLOCK ***** *)


{ This include file is suggested by Andre Felix Miertschink to
  make this program could be compiled by Delphi XE. }
  
{$I ..\..\Source\GraphicsMagic.inc}

uses
{$IFDEF USE_FASTMM}
  FastMM,              // FastMM lib memory manager
{$ENDIF}
{$IFDEF USE_FASTCODE}
  FastCode,            // FastCode lib RTL Speedups
{$ENDIF}
{$IFDEF USE_FASTMOVE}
  FastMove,            // FastMove replaces rtl Move with lighling fast version
{$ENDIF}
{$IFDEF USE_VCLFIXPACK}
  VCLFixPack,          // Will fix many RTL/VCL Bugs and adds some speedups
{$ENDIF}
{$IFDEF USE_TRLVCLOPTIMIZE}
  RtlVclOptimize,      // Will add some Speedups into the Rtl and VCL
{$ENDIF}
  SysUtils,
  Forms,
  MainForm in 'MainForm.pas' {frmMain},
  NilsHaeckBeziers in '..\..\GraphicsMagicLib\NilsHaeckBeziers.pas',
  GhostForm in 'GhostForm.pas' {frmGhost},
  MainDataModule in '..\..\Source\MainDataModule.pas' {dmMain: TDataModule},
  ChildForm in '..\..\Source\ChildForm.pas' {frmChild},
  HistoryDataModule in '..\..\Source\HistoryDataModule.pas' {dmHistory: TDataModule},
  LayerDataModule in '..\..\Source\LayerDataModule.pas' {dmLayer: TDataModule},
  InfoForm in '..\..\Source\InfoForm.pas' {frmInfo},
  HistoryForm in '..\..\Source\HistoryForm.pas' {frmHistory},
  RichTextEditorForm in '..\..\Source\RichTextEditorForm.pas' {frmRichTextEditor},
  ColorForm in '..\..\Source\ColorForm.pas' {frmColor},
  SwatchDataModule in '..\..\Source\SwatchDataModule.pas' {dmSwatches: TDataModule},
  ColorSwatchNameDlg in '..\..\Source\ColorSwatchNameDlg.pas' {frmColorSwatchName},
  GradientPickerPopFrm in '..\..\Source\GradientPickerPopFrm.pas' {frmGradientPicker},
  RenameGradientDlg in '..\..\Source\RenameGradientDlg.pas' {frmGradientName},
  GradientMapDlg in '..\..\Source\GradientMapDlg.pas' {frmGradientMap},
  GradientEditorDlg in '..\..\Source\GradientEditorDlg.pas' {frmGradientEditor},
  GradientFillDlg in '..\..\Source\GradientFillDlg.pas' {frmGradientFill},
  LayerForm in '..\..\Source\LayerForm.pas' {frmLayer},
  ChannelForm in '..\..\Source\ChannelForm.pas' {frmChannel},
  PathForm in '..\..\Source\PathForm.pas' {frmPath},
  SplashForm in '..\..\Source\SplashForm.pas' {frmSplash},
  PatternsPopFrm in '..\..\Source\PatternsPopFrm.pas' {frmPatterns},
  PatternNameDlg in '..\..\Source\PatternNameDlg.pas' {frmPatternName},
  FillDlg in '..\..\Source\FillDlg.pas' {frmFill},
  PatternFillDlg in '..\..\Source\PatternFillDlg.pas' {frmPatternFill},
  PaintingBrushPopFrm in '..\..\Source\PaintingBrushPopFrm.pas' {frmPaintingBrush},
  BrushDynamicsPopFrm in '..\..\Source\BrushDynamicsPopFrm.pas' {frmBrushDynamics},
  PaintBucketOptionsPopFrm in '..\..\Source\PaintBucketOptionsPopFrm.pas' {frmPaintBucketAdvancedOptions},
  EraserAdvOptionsPopFrm in '..\..\Source\EraserAdvOptionsPopFrm.pas' {frmEraserAdvancedOptions},
  NewFileDlg in '..\..\Source\NewFileDlg.pas' {frmCreateNewFile},
  PrintPreviewDlg in '..\..\Source\PrintPreviewDlg.pas' {frmPrintPreview},
  PrintOptionsDlg in '..\..\Source\PrintOptionsDlg.pas' {frmPrintOptions},
  ColorBalanceDlg in '..\..\Source\ColorBalanceDlg.pas' {frmColorBalance},
  HueSaturationDlg in '..\..\Source\HueSaturationDlg.pas' {frmHueSaturation},
  BrightnessContrastDlg in '..\..\Source\BrightnessContrastDlg.pas' {frmBrightnessContrast},
  ReplaceColorDlg in '..\..\Source\ReplaceColorDlg.pas' {frmReplaceColor},
  ThresholdDlg in '..\..\Source\ThresholdDlg.pas' {frmThreshold},
  PosterizeDlg in '..\..\Source\PosterizeDlg.pas' {frmPosterize},
  FeatherSelectionDlg in '..\..\Source\FeatherSelectionDlg.pas' {frmFeatherSelection},
  ColorRangeSelectionDlg in '..\..\Source\ColorRangeSelectionDlg.pas' {frmColorRangeSelection},
  CurvesDlg in '..\..\Source\CurvesDlg.pas' {frmCurves},
  LevelsToolDlg in '..\..\Source\LevelsToolDlg.pas' {frmLevelsTool},
  ImageColorPickerForm in '..\..\Source\ImageColorPickerForm.pas' {frmImageColorPicker},
  ChannelMixerDlg in '..\..\Source\ChannelMixerDlg.pas' {frmChannelMixer},
  ImageSizeDlg in '..\..\Source\ImageSizeDlg.pas' {frmImageSize},
  CanvasSizeDlg in '..\..\Source\CanvasSizeDlg.pas' {frmCanvasSize},
  RotateCanvasDlg in '..\..\Source\RotateCanvasDlg.pas' {frmRotateCanvas},
  IndexedColorDlg in '..\..\Source\IndexedColorDlg.pas' {frmIndexedColor},
  HistogramDlg in '..\..\Source\HistogramDlg.pas' {frmHistogram},
  DuplicateLayerDlg in '..\..\Source\DuplicateLayerDlg.pas' {frmDuplicateLayer},
  LayerPropertiesDlg in '..\..\Source\LayerPropertiesDlg.pas' {frmLayerProperties},
  AboutDlg in '..\..\Source\AboutDlg.pas' {frmAbout},
  LicenceDlg in '..\..\Source\LicenceDlg.pas' {frmLicence},
  CreditsDlg in '..\..\Source\CreditsDlg.pas' {frmCredits},
  PreferencesDlg in '..\..\Source\PreferencesDlg.pas' {frmPreferences},
  ApplyImageDlg in '..\..\Source\ApplyImageDlg.pas' {frmApplyImage},
  ChannelOptionsDlg in '..\..\Source\ChannelOptionsDlg.pas' {frmChannelOptions},
  SelectFiguresDlg in '..\..\Source\SelectFiguresDlg.pas' {frmSelectFigures},
  FigurePropertiesDlg in '..\..\Source\FigurePropertiesDlg.pas' {frmFigureProperties},
  ChannelDataModule in '..\..\Source\ChannelDataModule.pas' {dmChannel: TDataModule},
  DuplicateChannelDlg in '..\..\Source\DuplicateChannelDlg.pas' {frmDuplicateChannel},
  SavePathDlg in '..\..\Source\SavePathDlg.pas' {frmSavePath},
  SwatchForm in '..\..\Source\SwatchForm.pas' {frmSwatch};

{$R *.RES}

begin
  frmSplash := TfrmSplash.Create(Application);
  frmSplash.ClickEnabled := False;
  frmSplash.Show;
  frmSplash.Update;
  Application.Initialize;
  Application.Title := 'GraphicsMagic Professional 1.4.7';
  Application.CreateForm(TdmMain, dmMain);
  Application.CreateForm(TdmHistory, dmHistory);
  Application.CreateForm(TdmSwatches, dmSwatches);
  Application.CreateForm(TdmLayer, dmLayer);
  Application.CreateForm(TdmChannel, dmChannel);
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmInfo, frmInfo);
  Application.CreateForm(TfrmHistory, frmHistory);
  Application.CreateForm(TfrmColor, frmColor);
  Application.CreateForm(TfrmSwatch, frmSwatch);
  Application.CreateForm(TfrmLayer, frmLayer);
  Application.CreateForm(TfrmChannel, frmChannel);
  Application.CreateForm(TfrmPath, frmPath);
  Application.CreateForm(TfrmPaintingBrush, frmPaintingBrush);
  Application.CreateForm(TfrmFill, frmFill);
  Application.CreateForm(TfrmPatternFill, frmPatternFill);
  Application.CreateForm(TfrmPatterns, frmPatterns);
  Application.CreateForm(TfrmBrushDynamics, frmBrushDynamics);
  Application.CreateForm(TfrmEraserAdvancedOptions, frmEraserAdvancedOptions);
  Application.CreateForm(TfrmPaintBucketAdvancedOptions, frmPaintBucketAdvancedOptions);
  Application.CreateForm(TfrmGradientMap, frmGradientMap);
  Application.CreateForm(TfrmGradientFill, frmGradientFill);
  Application.CreateForm(TfrmGradientPicker, frmGradientPicker);
  Application.CreateForm(TfrmPrintPreview, frmPrintPreview);
  Application.CreateForm(TfrmRichTextEditor, frmRichTextEditor);
  Application.CreateForm(TfrmPrintOptions, frmPrintOptions);
  Application.CreateForm(TfrmCredits, frmCredits);
  frmSplash.Hide;
  FreeAndNil(frmSplash);  
  Application.Run;
end.
