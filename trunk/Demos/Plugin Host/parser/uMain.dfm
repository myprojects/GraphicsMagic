object frmMain: TfrmMain
  Left = 276
  Top = 215
  Width = 729
  Height = 480
  Caption = 'frmMain'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object spl1: TSplitter
    Left = 520
    Top = 0
    Height = 442
    Align = alRight
  end
  object pnl: TPanel
    Left = 523
    Top = 0
    Width = 190
    Height = 442
    Align = alRight
    Color = clWindow
    UseDockManager = False
    TabOrder = 0
  end
  object pnl1: TPanel
    Left = 0
    Top = 0
    Width = 520
    Height = 442
    Align = alClient
    BevelOuter = bvNone
    BorderWidth = 10
    TabOrder = 1
    object pnl2: TPanel
      Left = 10
      Top = 239
      Width = 500
      Height = 65
      Align = alBottom
      BevelOuter = bvNone
      TabOrder = 0
      object btn1: TButton
        Left = 32
        Top = 16
        Width = 75
        Height = 25
        Caption = 'Parse'
        TabOrder = 0
        OnClick = btn1Click
      end
      object TrackBar1: TTrackBar
        Left = 144
        Top = 24
        Width = 150
        Height = 45
        TabOrder = 1
        TickMarks = tmTopLeft
      end
      object RadioButton1: TRadioButton
        Left = 344
        Top = 32
        Width = 113
        Height = 17
        Caption = 'RadioButton1'
        TabOrder = 2
      end
      object btnVals: TButton
        Left = 416
        Top = 24
        Width = 75
        Height = 25
        Caption = 'btnVals'
        TabOrder = 3
        OnClick = btnValsClick
      end
    end
    object pgctl1: TPageControl
      Left = 10
      Top = 10
      Width = 500
      Height = 229
      ActivePage = ts2
      Align = alClient
      TabOrder = 1
      object ts1: TTabSheet
        Caption = 'ts1'
        object Memo1: TMemo
          Left = 0
          Top = 0
          Width = 492
          Height = 201
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            '[Emboss]'
            #9'Channel:  [___|v]'
            #9'Function:'
            #9'(*) Bumpmap  ( ) Emboss'
            #9'Azimut:  [$]'
            #9'[<0~360>]'
            #9'Elevation:  [$]'
            #9'[<0~180>]'
            #9'Depth:  [$]'
            #9'[<0~100>]'
            '#=Emboss( Channel=0,'#9'Function=1, Azimut=90, '
            'Elevation=120, '
            'Depth=50 );')
          ParentFont = False
          TabOrder = 0
        end
      end
      object ts2: TTabSheet
        Caption = 'ts2'
        ImageIndex = 1
        object mmo1: TMemo
          Left = 0
          Top = 0
          Width = 492
          Height = 201
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            '[Add Noise]'
            #9'Amount:'
            #9'[<0~400>]  $'
            #9'[x] Monocromatic'
            '=Add_Noise( Amount=30, '#9'Monocromatic=1);')
          ParentFont = False
          TabOrder = 0
        end
      end
      object ts3: TTabSheet
        Caption = 'Adjust color'
        ImageIndex = 2
        object mmo2: TMemo
          Left = 0
          Top = 0
          Width = 492
          Height = 201
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            '[Adjust color]'
            #9'(*) Auto adjust'
            #9'( ) Exposure'
            #9'Exposure adjustment'
            #9'[<0~100>]'
            #9'( ) Manual adjustment'
            #9'Brightness'
            #9'[<0~30>]'
            #9'Contrast'
            #9'[<0~255>]'
            #9'Saturation'
            #9'[<0~100>]'
            #9'( ) Grayscale (Hybrid)'
            #9'( ) Grayscale (Average)'
            #9'( ) Grayscale (Lightness)')
          ParentFont = False
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
      object ts4: TTabSheet
        Caption = 'ts4'
        ImageIndex = 3
        object mmo3: TMemo
          Left = 0
          Top = 0
          Width = 492
          Height = 201
          Align = alClient
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Courier New'
          Font.Style = []
          Lines.Strings = (
            '[Color Balance2]'
            #9'[Color Levels'
            #9#9'Cyan / Red  [$]'
            #9#9'[<-100~100>]  '
            #9#9'Magenta / Green  [$]'
            #9#9'[<-100~100>]  '
            #9#9'Yellow / Blue  [$]'
            #9#9'[<-100~100>] '
            #9']'
            #9'[Tone Balance'
            #9#9'( ) Shadows  (*) Midtones  ( ) Highlights'
            #9#9'[x] Preserve Luminosity'
            #9']')
          ParentFont = False
          ScrollBars = ssBoth
          TabOrder = 0
        end
      end
    end
    object pgctl2: TPageControl
      Left = 10
      Top = 304
      Width = 500
      Height = 128
      ActivePage = ts6
      Align = alBottom
      TabOrder = 2
      TabPosition = tpBottom
      object ts5: TTabSheet
        Caption = 'ts5'
        object lst1: TListBox
          Left = 0
          Top = 0
          Width = 492
          Height = 102
          Align = alClient
          ItemHeight = 13
          TabOrder = 0
        end
      end
      object ts6: TTabSheet
        Caption = 'ts6'
        ImageIndex = 1
        object mmoFunc: TMemo
          Left = 0
          Top = 0
          Width = 320
          Height = 102
          Align = alClient
          Lines.Strings = (
            'mmoFunc')
          TabOrder = 0
        end
        object vlst1: TValueListEditor
          Left = 320
          Top = 0
          Width = 172
          Height = 102
          Align = alRight
          Strings.Strings = (
            
              'Channel=0, Bumpmap=False, Emboss=True, Azimut=99, Elevation=38, ' +
              'Depth=11')
          TabOrder = 1
          ColWidths = (
            119
            47)
        end
      end
    end
  end
end
