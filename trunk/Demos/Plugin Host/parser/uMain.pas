unit uMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  XPMan,
  Dialogs, StdCtrls, ExtCtrls, ComCtrls,
  uParser, gmFilterBridges, Grids, ValEdit;

type
  TfrmMain = class(TForm)
    pnl: TPanel;
    pnl1: TPanel;
    pnl2: TPanel;
    btn1: TButton;
    pgctl1: TPageControl;
    ts1: TTabSheet;
    Memo1: TMemo;
    ts2: TTabSheet;
    mmo1: TMemo;
    ts3: TTabSheet;
    mmo2: TMemo;
    spl1: TSplitter;
    TrackBar1: TTrackBar;
    ts4: TTabSheet;
    mmo3: TMemo;
    pgctl2: TPageControl;
    ts5: TTabSheet;
    ts6: TTabSheet;
    lst1: TListBox;
    mmoFunc: TMemo;
    RadioButton1: TRadioButton;
    vlst1: TValueListEditor;
    btnVals: TButton;
    procedure btn1Click(Sender: TObject);
    procedure btnValsClick(Sender: TObject);
  private
    { Private declarations }
    FUIParams : TgmFilterBridge;
  public
    { Public declarations }
    procedure GUIChanged(Sender : TObject);
  end;

var
  frmMain: TfrmMain;

implementation
//uses uParser, gmFilterBridges;
{$R *.dfm}

procedure TfrmMain.btn1Click(Sender: TObject);
  procedure ClearCtrls();
  var i : Integer;
  begin
    for i := pnl.ControlCount -1 downto 0 do
    begin
      pnl.Controls[i].Free;
    end;

  end;

var m : TMemo;
  c : TControl;
  I,j : Integer;
  par : TgmFilterBridge;
  //ca : TControlsArray;
  //cl : TList;
  fp : TgmFilterParam;
begin
  ClearCtrls();
  lst1.clear();
  c := pgctl1.ActivePage.Controls[0];
  m := TMemo(c);
  par := Parse(m.Lines.Text, pnl); FUIParams := par;
  par.OnChange := GUIChanged;
  for i := 0 to par.Count-1 do
  begin
    {ca := TControlsArray(par.Objects[i]);
    for j := 0 to Length(ca)-1 do
      lst1.Items.Add(Format('%s: %s',[ par[i], ca[j].ClassName ] ));}
    {cl := TList( par.Objects[i]);
    for j := 0 to cl.Count -1 do
    begin
      lst1.Items.Add(Format('%s: %s',[ par[i], TObject(cl.Items[j]).ClassName ] ));
    end;}
    lst1.Items.Add(par[i]);
    
    fp := TgmFilterParam( par.Objects[i]);
    for j := 0 to fp.Controls.Count -1 do
    begin
      lst1.Items.Add(Format('---"%s": %s',[ par[i], TObject(fp.Controls[j]).ClassName ] ));
    end;


  end;

end;

procedure TfrmMain.GUIChanged(Sender: TObject);
begin
  Caption := FormatDateTime('hh:mm:ss.zzz',now);
  mmoFunc.Lines.Text := FUIParams.AsString;
end;

procedure TfrmMain.btnValsClick(Sender: TObject);
begin
  vlst1.Strings.CommaText := mmoFunc.Text;
end;

end.
