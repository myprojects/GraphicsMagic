object frmDlg: TfrmDlg
  Left = 127
  Top = 294
  BorderStyle = bsDialog
  Caption = 'frmDlg'
  ClientHeight = 215
  ClientWidth = 394
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 13
  object pnl1: TPanel
    Left = 292
    Top = 0
    Width = 102
    Height = 215
    Align = alRight
    TabOrder = 0
    object btbtnOK: TBitBtn
      Left = 15
      Top = 16
      Width = 75
      Height = 25
      Cursor = crHandPoint
      TabOrder = 0
      Kind = bkOK
    end
    object btbtnCancel: TBitBtn
      Left = 15
      Top = 56
      Width = 75
      Height = 25
      Cursor = crHandPoint
      TabOrder = 1
      Kind = bkCancel
    end
    object chckbxPreview: TCheckBox
      Left = 15
      Top = 104
      Width = 75
      Height = 17
      Cursor = crHandPoint
      Caption = 'Preview'
      Checked = True
      State = cbChecked
      TabOrder = 2
      OnClick = chckbxPreviewClick
    end
  end
end
