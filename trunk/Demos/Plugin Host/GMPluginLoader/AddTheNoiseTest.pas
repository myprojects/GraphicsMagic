unit AddTheNoiseTest;

interface
uses
  FastCode,
  FastMove,
  VCLFixPack,
  RtlVclOptimize,
  SysUtils,
  Classes,
  Forms,
  Controls,
  GR32,
  StrUtils,
  //AddNoiseDlg in 'AddNoiseDlg.pas' {frmAddNoise},
  gmTypes, // in '..\..\..\GraphicsMagicLib\gmTypes.pas',
  gmPluginFuncs, // in '..\..\..\GraphicsMagicLib\gmPluginFuncs.pas',
  gmFilterBridges // in '..\parser\gmFilterBridges.pas'
  ;

type
  TUpdateViewProc = procedure;

var
  SelectedChannelSet: TgmChannelSet;

function ExecuteFilter(AppHandle: THandle; DestBmpPtr: PColor32;
  const Width, Height: Integer; //UpdateViewProc: TUpdateViewProc;
  ParamsStr: PChar;
  const BKColor: TColor32 = $00000000 ): Boolean;


implementation

uses gmAddNoise, gmImageProcessFuncs;


procedure AddNoiseExecuteAddNoise(FSourceBmp: TBitmap32;
  FChannelSet: TgmChannelSet;
  //FUpdateViewProc: TUpdateViewProc;
  FDestBmpPtr: PColor32;
  AParamsStr: string);
var
  LColorChannelCount: Integer;
  LParams : TgmParamArray;
  FProcessedBmp : TBitmap32;
{  FSourceBmp    := TBitmap32.Create;
  FProcessedBmp := TBitmap32.Create;

  FDestBmpPtr     := nil;
  FUpdateViewProc := nil;
  FAllowChange    := True;
  FChannelSet     := [csRed, csGreen, csBlue];
}
begin
  Screen.Cursor := crHourGlass;
  LParams := ParamArray(AParamsStr);
  FProcessedBmp := TBitmap32.Create;
  try
    FProcessedBmp.Assign(FSourceBmp);

    if LParams['Monochromatic'] = False then
    //case FNoiseMode of
      //nmColor:
        begin
          //AddColorNoise32(FProcessedBmp, ggbrAmount.Position);
          AddColorNoise32(FProcessedBmp, LParams['Amount']);
        end
    else
      //nmMono:
        begin
          //AddMonoNoise32(FProcessedBmp, ggbrAmount.Position);
          AddMonoNoise32(FProcessedBmp, LParams['Amount']);
        end;
    //end;

    if csGrayscale in FChannelSet then
    begin
      Desaturate32(FProcessedBmp);
    end
    else
    begin
      LColorChannelCount := GetColorChannelCount(FChannelSet);

      if (LColorChannelCount > 0) and (LColorChannelCount < 3) then
      begin
        ReplaceRGBChannels(FSourceBmp, FProcessedBmp, FChannelSet, crsRemainDest);
      end;
    end;

    //if chckbxPreview.Checked then
    begin
      CopyBmpDataToPtr(FProcessedBmp, FDestBmpPtr, FProcessedBmp.Width, FProcessedBmp.Height);

      {if Assigned(FUpdateViewProc) then
      begin
        FUpdateViewProc;
      end;}
    end;
  finally
    Screen.Cursor := crDefault;
    FProcessedBmp.Free;
    LParams.Free;
  end;
end;

function ExecuteFilter(AppHandle: THandle; DestBmpPtr: PColor32;
  const Width, Height: Integer; //UpdateViewProc: TUpdateViewProc;
  ParamsStr: PChar;
  const BKColor: TColor32 = $00000000 ): Boolean; //stdcall;
var
  LOldAppHandle: THandle;
  LIsSuccessed : Boolean;
  FSourceBmp : TBitmap32;
  s : string;
begin
  LIsSuccessed  := True;
  LOldAppHandle := Application.Handle;

  //Application.Handle := AppHandle;
  SelectedChannelSet := [csRed, csGreen, csBlue];


  //--------start
  FSourceBmp := TBitmap32.Create;

  //frmAddNoise        := TfrmAddNoise.Create(Application);  // create and open the setup dialog of this filter
  try
    CopyBmpDataFromPtr(DestBmpPtr, Width, Height, FSourceBmp);

    s := ParamsStr;
    AddNoiseExecuteAddNoise(FSourceBmp,
      SelectedChannelSet, //FChannelSet: TgmChannelSet;
      //UpdateViewProc, //FUpdateViewProc: TUpdateViewProc;
      DestBmpPtr, //FDestBmpPtr: PColor32;
      //ParamsStr
      s
      );

    {
    try
      frmAddNoise.FDestBmpPtr     := DestBmpPtr;
      frmAddNoise.FUpdateViewProc := UpdateViewProc;
      frmAddNoise.FChannelSet     := SelectedChannelSet;
      CopyBmpDataFromPtr(DestBmpPtr, Width, Height, frmAddNoise.FSourceBmp);

      if frmAddNoise.ShowModal = mrOK then
      begin
        if not frmAddNoise.chckbxPreview.Checked then
        begin
          CopyBmpDataToPtr(frmAddNoise.FProcessedBmp, DestBmpPtr, Width, Height);

          if Assigned(UpdateViewProc) then
          begin
            UpdateViewProc;
          end;
        end;
      end
      else
      begin
        CopyBmpDataToPtr(frmAddNoise.FSourceBmp, DestBmpPtr, Width, Height);

        if Assigned(UpdateViewProc) then
        begin
          UpdateViewProc;
        end;

        LIsSuccessed := False;
      end;

    except
      LIsSuccessed := False;
    end;
     }
  finally
    //frmAddNoise.Free;
    //Application.Handle := LOldAppHandle;
    FSourceBmp.Free;
  end;

  Result := LIsSuccessed;
end;
end.
