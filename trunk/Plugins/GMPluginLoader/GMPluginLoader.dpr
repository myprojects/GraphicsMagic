{ This program is for testing how to load/run the .gmp plugins which
  specifically designed for GraphicsMagic.

  CopyRight(C) 2001-2008, Ma Xiaoguang & Ma Xiaoming < gmbros@hotmail.com >.
  All rights reserved. }

program GMPluginLoader;

{ This include file is suggested by Andre Felix Miertschink to
  make this program could be compiled by Delphi XE. }

{$I ..\..\GraphicsMagic.inc}

uses
{$IFDEF USE_FASTCODE}
  FastCode,            // FastCode lib RTL Speedups
{$ENDIF}
{$IFDEF USE_FASTMOVE}
  FastMove,            // FastMove replaces rtl Move with lighling fast version
{$ENDIF}
{$IFDEF USE_VCLFIXPACK}
  VCLFixPack,          // Will fix many RTL/VCL Bugs and adds some speedups
{$ENDIF}
{$IFDEF USE_TRLVCLOPTIMIZE}
  RtlVclOptimize,      // Will add some Speedups into the Rtl and VCL
{$ENDIF}
  Forms,
  Main in 'Main.pas' {frmMain};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
